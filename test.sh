set -e
new_version="$1"
domains="$2"
user_name="manas_bhardwaj"
bitbucket_key="RS2djuJr3DjYgf26LehY"
workspace="cleartrip"
user_email="manas.bhardwaj@cleartrip.com(opens in new tab)"
pattern_1="com.cleartrip.analytics.protofiles:[0-9]*.[0-9]*.[0-9]*"
pattern_2=$pattern_1"_RELEASE"
pattern_3="'com.cleartrip.analytics'[[:space:]]*,[[:space:]]*name[[:space:]]*:[[:space:]]*'protofiles'[[:space:]]*,[[:space:]]*version[[:space:]]*:[[:space:]]*'[0-9]*\.[0-9]*\.[0-9]*'"
pattern_4="'com.cleartrip.analytics'[[:space:]]*,[[:space:]]*name[[:space:]]*:[[:space:]]*'protofiles'[[:space:]]*,[[:space:]]*version[[:space:]]*:[[:space:]]*'[0-9]*\.[0-9]*\.[0-9]*_RELEASE'"
updated_pattern_1="com.cleartrip.analytics:protofiles:${new_version}"
updated_pattern_2="'com.cleartrip.analytics',name:'protofiles',version:'${new_version}'"
branch="release/prod"
pipeline_name="STORM_DEPLOYMENTS_GCP"
environment="gcp"
task="deploy"
repositories="realtime-processing bq"
# git config --global user.name \""$user_name"\"
# git config --global user.email \""$user_email"\"
# git config --global push.default simple
for repository in $repositories; do
  url="https://api.bitbucket.org/2.0/repositories/${workspace}/${repository}/pipelines/"
  git clone https://"$user_name":"$bitbucket_key"@bitbucket.org/"$workspace"/"$repository".git
  cd $repository
  git checkout "$branch"
  gradlePaths=$(find . -name "build.gradle")
  for file in $gradlePaths ; do
      is_updated=0
      should_deploy=-1
      should_alert=0
    if [ "$(grep "protofiles" -c "$file")" -gt 0 ]; then
        if [ "$(grep "org.apache.storm" -c "$file")" -gt 0 ] && [ "$(grep "storm-core" -c "$file")" -gt 0 ] ; then
          should_deploy=1
          project_root=$(echo "$file" | rev | cut -d '/' -f 2 | rev)
        fi
        for domain in $domains; do
          if [ "$(echo "$file" | grep -i "$domain" -c)" -gt 0 ] ; then
            should_deploy=0
            should_alert=1
          fi
        done
      echo "========= Checking protofile version pattern in $file ========="
      if [[ $(sed -n "/$pattern_2/p" "$file" | wc -m ) -gt 0 ]]; then
        sed -i "s/$pattern_2/$updated_pattern_1/g" "$file"
        is_updated=1

      elif [[ $(sed -n "/$pattern_1/p" "$file" | wc -m ) -gt 0 ]]; then
        sed -i "s/$pattern_1/$updated_pattern_1/g" "$file"
        is_updated=1

      elif [[ $(sed -n "/$pattern_4/p" "$file" | wc -m ) -gt 0 ]]; then
        sed -i "s/$pattern_4/$updated_pattern_2/g" "$file"
        is_updated=1

      elif [[ $(sed -n "/$pattern_3/p" "$file" | wc -m ) -gt 0 ]]; then
        sed -i "s/$pattern_3/$updated_pattern_2/g" "$file"
        is_updated=1
    else
        data="Pattern didn't matched in repository : $repository having build.gradle path '$file' please check manually and let the team know for changes."
        echo $data
        # curl -X POST -H 'Content-type: application/json' --data "{'text':'$data'}" https://hooks.slack.com/services/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
        should_deploy=0
      fi
    else
      echo "========= No PROTOFILES dependency found in $file. ========="
      should_deploy=0
    fi
#    if [[ "$is_updated" -eq 1 ]]; then
#      git status
    #   git add .
    #   git commit -m "Update Protoversion via script."
    #   echo "================= Pushing changes in $repository ================="
    #   git push https://"$user_name":"$bitbucket_key"@bitbucket.org/"$workspace"/"$repository".git
#    fi
    if [[ "$is_updated" -eq 1 ]] && [[ "$should_deploy" -eq 0 ]] && [[ "$should_alert" -eq 1 ]] ; then
      project_root=$(echo "$file" | rev | cut -d '/' -f 2 | rev)
      if [[ "$project_root" == "." ]]; then
        project_root=$repository
      fi
      data="Protoversion has been updated. Please manually check the changes that should be made in repository : $repository in project $project_root apart from protofile version change."
      echo $data
    #   curl -X POST -H 'Content-type: application/json' --data "{'text':'$data'}" https://hooks.slack.com/services/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
    fi

    if [[ "$is_updated" -eq 1 ]] && [[ "$should_deploy" -eq 1 ]] && [[ "$should_alert" -eq 0 ]]; then
      project_root=$(echo "$file" | cut -c3- | rev | cut -c14- | rev)
      request_body="'{ \"target\": {
            \"type\": \"pipeline_ref_target\",
            \"ref_type\": \"branch\",
            \"ref_name\": \"${branch}\",
            \"selector\": {
              \"type\": \"custom\",
              \"pattern\": \"${pipeline_name}\"
            }
          },
          \"variables\": [
            {
              \"key\": \"PROJECT\",
              \"value\": \"${project_root}\"
            },
            {
              \"key\": \"ENVIRONMENT\",
              \"value\": \"$environment\"
            },
            {
              \"key\": \"TASK\",
              \"value\": \"$task\"
            }
          ]
        }'"
    #   echo "=== run bitbucket pipelines for $project_root $environment ==="
      cmd="curl -X POST -is -u ${user_name}:${bitbucket_key} -H 'Content-Type: application/json' ${url} -d ${request_body}"
      echo $cmd
    #   eval "$cmd"
    #   echo
    #   echo "=== successfully triggered bitbucket pipeline ${pipeline_name} in ${repository}"
      data="Successfully triggered bitbucket pipeline ${pipeline_name} in repository ${repository} on branch $branch with variables as PROJECT=$project_root, ENVIRONMENT=$environment, TASK=$task "
      echo $data
    #   curl -X POST -H 'Content-type: application/json' --data "{'text':'$data'}" https://hooks.slack.com/services/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
    fi
  done
  echo
  echo
  echo
  echo
done
