import os
import re
import sys


def get_gradlebuild_list(path="."):
    # Get the list of all build.gradle in directory tree at given path
    fileList = list()
    for (dirpath, dirnames, filenames) in os.walk(path):
        fileList += [os.path.join(dirpath, file) for file in filenames if file == "build.gradle"]
    return fileList


def getProtoUsingRepoList(env=''):
    # Gets the repo that uses protofiles.
    lst = get_gradlebuild_list(".")
    repoList = list()
    for filePath in lst:
        with open(filePath, 'r') as file:
            filedata = file.read()

        pattern = re.compile("protofiles")
        dependency_list = re.findall(pattern, str(filedata))
        if len(dependency_list) != 0:
            #             print(dependency_list)
            repoList.append(filePath)
    all_repo = ['aws_emr_scripts', 'benchmarking-projects', 'bq', 'cloud-function', 'data-devops', 'fare-alert',
                'hadoop-loader', 'hotel-sort-order', 'lake', 'nurture', 'page-performance', 'protofiles', 'ptb',
                'ptb-models', 'python-scripts', 'realtime-processing', 'redshift2bq', 'reward-program', 'rundeck',
                'sink', 'sinkext', 'source', 'state-machine', 'stats-ui', 'tools', 'userclassification',
                'user-profiling', 'utils']
    repoList = list(set(repoList))
    repoList = list(set([repo.split('/')[1] for repo in repoList if repo.split('/')[1] in all_repo]))

    if env == "testing":
        return ['spring-producer', 'storm-consumer']
    elif len(repoList) <= 14:
        return ['hadoop-loader', 'sink', 'hotel-sort-order', 'reward-program',
                'test', 'page-performance', 'sinkext', 'user-profiling', 'fare-alert',
                'tools', 'ptb', 'realtime-processing', 'BQ', 'state-machine']
    else:
        return repoList


def replace_proto_version(fileList, update_version, repo):
    # Update protoversion in build.gradle

    is_updated = 0

    for filePath in fileList:
        with open(filePath, 'r') as file:
            filedata = file.read()
        #         print(filedata)

        pattern = re.compile(
            "'\s*com\.cleartrip\.analytics\s*'\s*,\s*name:\s*'protofiles'\s*,\s*version\s*:\s*'\s*[0-9]+\.[0-9]+\.[0-9]+_RELEASE\s*'")
        dependency_list = re.findall(pattern, str(filedata))
        if len(dependency_list) != 0 and is_updated == 0:
            old_version = dependency_list[0]
            new_version = "'com.cleartrip.analytics',name:'protofiles',version:'{}'".format(update_version)
            is_updated = 1

        pattern = re.compile(
            "'\s*com\.cleartrip\.analytics\s*'\s*,\s*name:\s*'protofiles'\s*,\s*version\s*:\s*'\s*[0-9]+\.[0-9]+\.[0-9]+\s*'")
        dependency_list = re.findall(pattern, str(filedata))
        if len(dependency_list) != 0 and is_updated == 0:
            old_version = dependency_list[0]
            new_version = "'com.cleartrip.analytics',name:'protofiles',version:'{}'".format(update_version)
            is_updated = 1

        pattern = re.compile("com\.cleartrip\.analytics:protofiles:[0-9]+\.[0-9]+\.[0-9]+_RELEASE")
        dependency_list = re.findall(pattern, str(filedata))
        if len(dependency_list) != 0 and is_updated == 0:
            old_version = dependency_list[0]
            new_version = "com.cleartrip.analytics:protofiles:{}".format(update_version)
            is_updated = 1

        pattern = re.compile("com\.cleartrip\.analytics:protofiles:[0-9]+\.[0-9]+\.[0-9]+")
        dependency_list = re.findall(pattern, str(filedata))
        if len(dependency_list) != 0 and is_updated == 0:
            old_version = dependency_list[0]
            new_version = "com.cleartrip.analytics:protofiles:{}".format(update_version)
            is_updated = 1

        if is_updated == 1:
            filedata = filedata.replace(old_version, new_version)
            #             print(filedata)
            with open(filePath, 'w') as file:
                file.write(filedata)
            os.system(
                "echo Successfully Changed the version in build.gradle in path : {} in repo {}.".format(filePath, repo))
            print("Successfully Changed the version in build.gradle in path : " + filePath + " in repo " + repo)
        else:
            os.system("echo Pattern didn't matched in : {}, Please check the build.gradle".format(filePath))
            print("Pattern didn't matched in : " + filePath + " , Please check the build.gradle")


# For production

if len(sys.argv) == 1:
    print("Please specify version of protofile")
    os.system("echo Please specify version of protofile")
    os._exit(1)
elif len(sys.argv) == 2:
    new_version = str(sys.argv[1])
else:
    os.systen("echo Usage : python <filname>.py <New version>")
    print("Usage : python <filname>.py <New version>")
    os._exit(1)

repo_list = getProtoUsingRepoList("testing")
for repo in repo_list:
    try:
        # For Testing.
        clone_url = "git clone https://manas_bhardwaj:RS2djuJr3DjYgf26LehY@bitbucket.org/manas_bhardwaj/" + repo + ".git"
        push_url = "git push https://manas_bhardwaj:RS2djuJr3DjYgf26LehY@bitbucket.org/manas_bhardwaj/" + repo + ".git"
        # For Production
        #         clone_url = "git clone https://manas_bhardwaj:RS2djuJr3DjYgf26LehY@bitbucket.org/cleartrip/" + repo + ".git"
        #         push_url = "git push https://manas_bhardwaj:RS2djuJr3DjYgf26LehY@bitbucket.org/cleartrip/" + repo + ".git"

        os.system('git config --global user.name "manas_bhardwaj"')
        os.system('git config --global user.email "manas.bhardwaj@cleartrip.com"')
        os.system('git config --global push.default simple')
        os.system(clone_url)
        os.chdir(repo)
        #         print(os.getcwd())
        os.system("git checkout release/prod")
        fileList = get_gradlebuild_list()
        replace_proto_version(fileList, new_version, repo)
        os.system("git status")
        for path in fileList:
            os.system("git add " + path)
        os.system('git commit -m "Update Protoversion via script." ')
        os.system(push_url)
        os.chdir("../")
        #         print(os.getcwd())
        os.system("echo Pushed updated protofile version of repo : " + repo)
        print("Pushed updated protofile version of repo : " + repo)
    except Exception as e:
        os.system("Error occured while changing proto version in : {} with error {} .".format(repo, repr(e)))
        print("Error occured while changing proto version in : " + repo + " with error " + repr(e))
